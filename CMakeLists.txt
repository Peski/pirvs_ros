cmake_minimum_required(VERSION 2.8.3)
project(pirvs_ros)

list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake_modules)

## Compile as C++11
add_compile_options(-std=c++11)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wpedantic -Wall -Wextra -O3 -march=native -DNDEBUG")
set(CMAKE_BUILD_TYPE "Release" CACHE STRING "Build type" FORCE)

## Find catkin macros and libraries
find_package(catkin REQUIRED COMPONENTS
  roscpp
# image_transport
  sensor_msgs
  cv_bridge
)

## System dependencies are found with CMake's conventions
find_package(Boost REQUIRED COMPONENTS thread filesystem system)
find_package(OpenCV REQUIRED)
find_package(libusb-1.0 REQUIRED)

## catkin specific configuration
catkin_package(
  # INCLUDE_DIRS include
  # LIBRARIES pirvs_ros
  # CATKIN_DEPENDS roscpp rospy std_msgs
  # DEPENDS system_lib
)

## Set PIRVS SDK library and headers
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/pirvs/include)

set(EXT_LIBS
  ${Boost_LIBRARIES}
  ${OpenCV_LIBS}
  ${LIBUSB_1_LIBRARIES}
  -lpthread
  -fopenmp
  -fPIC
  -lblas
  -llapack
)

add_library(pirvs INTERFACE)
target_include_directories(pirvs INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}/pirvs/include)
target_link_libraries(pirvs INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}/pirvs/lib/libPerceptInPIRVS.a ${EXT_LIBS})

## Build
include_directories(
  include
  ${catkin_INCLUDE_DIRS}
  ${Boost_INCLUDE_DIRS}
  ${OpenCV_INCLUDE_DIRS}
)

add_executable(pirvs_publisher_node src/pirvs_publisher_node.cpp src/pirvs_publisher.cpp src/single_subscriber_publisher.cpp)
target_link_libraries(pirvs_publisher_node ${catkin_LIBRARIES} pirvs)
