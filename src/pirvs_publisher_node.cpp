
#include <pthread.h>
#include <sched.h>
#include <signal.h>

#include <memory>
#include <thread>

#include <ros/ros.h>

#include <opencv2/core.hpp>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

#include <sensor_msgs/Image.h>
#include <sensor_msgs/Imu.h>

#include <pirvs.h>
#include "pirvs_publisher.h"

#include "circular_buffer.hpp"
#include "timer.hpp"

#define Q_SIZE_STEREOIMAGE 100
//#define Q_SIZE_CAMERAINFO 1
#define Q_SIZE_IMU 5000

std::shared_ptr<PIRVS::PerceptInDevice> gDevice = nullptr;
CircularBuffer<std::shared_ptr<const PIRVS::ImuData>> gBufferImu(4096);
CircularBuffer<std::shared_ptr<const PIRVS::StereoData>> gBufferStereo(1024);

/**
 * (Un)Gracefully exit when CTRL-C is hit
 */
void exit_handler(int sig) {
    (void) sig;

    if (gDevice != nullptr) {
        gDevice->StopDevice();
    }

    ros::shutdown();
    std::terminate();
}

/**
 * Tries to set realtime priority (requires superuser privileges)
 * Adapted from: http://www.yonch.com/tech/82-linux-thread-priority
 */ 
void set_realtime_priority() {
     int ret;

     // We'll operate on the currently running thread.
     pthread_t this_thread = pthread_self();

     // struct sched_param is used to store the scheduling priority
     struct sched_param params;

     // We'll set the priority to the maximum.
     int priority = sched_get_priority_max(SCHED_FIFO);
     params.sched_priority = priority;
     //std::cout << "Trying to set thread realtime prio = " << params.sched_priority << std::endl;

     // Attempt to set thread real-time priority to the SCHED_FIFO policy
     ret = pthread_setschedparam(this_thread, SCHED_FIFO, &params);
     if (ret != 0) {
         // Print the error
         //std::cout << "Unsuccessful in setting thread realtime prio" << std::endl;
         ROS_WARN("Unable to set realtime thread priority!");
         return;     
     }

     // Now verify the change in thread priority
     int policy = 0;
     ret = pthread_getschedparam(this_thread, &policy, &params);
     if (ret != 0) {
         ROS_WARN("Unable to verify thread priority!");
         return;
     }

     // Check the correct policy was applied
     if(policy != SCHED_FIFO || params.sched_priority != priority) {
         ROS_WARN("Unable to set realtime thread priority!");
     }

     // Print thread scheduling priority
     //std::cout << "Thread priority is " << params.sched_priority << std::endl; 
     ROS_INFO("Realtime thread priority set");
}

/**
 * Create IMU Message
 */
inline void CreateIMUMsg(sensor_msgs::Imu *imu_msg,
                         std::shared_ptr<const PIRVS::ImuData> imu_data,
                         const std::string& frame_id, const ros::Time& timestamp) {
    if (!imu_data) {
        return;
    }

    imu_msg->header.stamp = timestamp;
    imu_msg->header.frame_id = frame_id;
    imu_msg->angular_velocity.x = imu_data->ang_v[0];
    imu_msg->angular_velocity.y = imu_data->ang_v[1];
    imu_msg->angular_velocity.z = imu_data->ang_v[2];
    imu_msg->linear_acceleration.x = imu_data->accel[0];
    imu_msg->linear_acceleration.y = imu_data->accel[1];
    imu_msg->linear_acceleration.z = imu_data->accel[2];
}

/**
 * Create Image Message
 */
inline void CreateImageMsg(sensor_msgs::ImagePtr *image_msg,
                           cv::Mat cv_image,
                           const std::string& frame_id, const ros::Time& timestamp) {
    std_msgs::Header image_header;
    image_header.stamp = timestamp;
    image_header.frame_id = frame_id;
    *image_msg = cv_bridge::CvImage(image_header, sensor_msgs::image_encodings::MONO8, cv_image).toImageMsg();
}

/**
 * Stereo image publisher thread
 */
void PublisherThreadImu() {
    ros::NodeHandle nh;

    ros::Publisher pub_imu = nh.advertise<sensor_msgs::Imu>("/pirvs/imu/data", Q_SIZE_IMU);

    for(;;) {
        std::shared_ptr<const PIRVS::ImuData> imu_data = gBufferImu.read();

        ros::Time timestamp((double)imu_data->timestamp / 1000.0);
        sensor_msgs::Imu imu_msg;
        CreateIMUMsg(&imu_msg, imu_data, "IMU", timestamp);
        pub_imu.publish(imu_msg);
    }
}

/**
 * Stereo image publisher thread
 */
void PublisherThreadStereo() {
    ros::NodeHandle nh;

    image_transport::PirvsPublisher pub_left;
    image_transport::PirvsPublisher pub_right;

    pub_left.advertise(nh, "/pirvs/camera_left/image_raw", Q_SIZE_STEREOIMAGE);
    pub_right.advertise(nh, "/pirvs/camera_right/image_raw", Q_SIZE_STEREOIMAGE);

    for(;;) {
        std::shared_ptr<const PIRVS::StereoData> stereo_data = gBufferStereo.read();

        ros::Time timestamp((double)stereo_data->timestamp / 1000.0);
/*
            cv::Mat cv_img_left = cv::Mat(stereo_data->img_l.rows, stereo_data->img_l.cols, CV_8UC1);
            memcpy(cv_img_left.data, stereo_data->img_l.data, cv_img_left.total() * cv_img_left.elemSize());

            cv::Mat cv_img_right = cv::Mat(stereo_data->img_r.rows, stereo_data->img_r.cols, CV_8UC1);
            memcpy(cv_img_right.data, stereo_data->img_r.data, cv_img_right.total() * cv_img_right.elemSize());
*/
        sensor_msgs::ImagePtr image_msg_left;
        sensor_msgs::ImagePtr image_msg_right;

        CreateImageMsg(&image_msg_left, stereo_data->img_l, "StereoLeft", timestamp);
        CreateImageMsg(&image_msg_right, stereo_data->img_r, "StereoRight", timestamp);

            //CreateImageMsg(&image_msg_left, cv_img_left, "StereoLeft", timestamp);
            //CreateImageMsg(&image_msg_right, cv_img_right, "StereoRight", timestamp);

        pub_left.publish(image_msg_left);
        pub_right.publish(image_msg_right);
    }
}

int main(int argc, char* argv[]) {

    struct sigaction sigIntHandler;
    sigIntHandler.sa_handler = exit_handler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);

    ros::init(argc, argv, "PirvsPublisherNode", ros::init_options::NoSigintHandler);
    //ros::NodeHandle nh;
    //ros::Publisher pub_imu = nh.advertise<sensor_msgs::Imu>("/pirvs/imu/data", Q_SIZE_IMU);

    // create publisher threads.
    std::thread imu_publisher(PublisherThreadImu);
    std::thread sterep_publisher(PublisherThreadStereo);

    set_realtime_priority();

    if (!PIRVS::CreatePerceptInV1Device(&gDevice) || !gDevice) {
        ROS_ERROR("Failed to create device.\n");
        return -1;
    }

    if (!gDevice->StartDevice()) {
        ROS_ERROR("Failed to start device.\n");
        return -1;
    }

    gDevice->SetExposure(600); // from 0 to 2000

    //Timer timer;

    for(;;) {
        std::shared_ptr<const PIRVS::Data> data;
        if (!gDevice->GetData(&data)) {
            continue;
        }

        std::shared_ptr<const PIRVS::ImuData> imu_data = std::dynamic_pointer_cast<const PIRVS::ImuData>(data);
        if (imu_data) gBufferImu.write(imu_data);
        else {
            std::shared_ptr<const PIRVS::StereoData> stereo_data = std::dynamic_pointer_cast<const PIRVS::StereoData>(data);
            if (stereo_data) {
                gBufferStereo.write(stereo_data);
/*
            ros::Time timestamp((double)stereo_data->timestamp / 1000.0);

            //cv::Mat cv_img_left = cv::Mat(stereo_data->img_l.rows, stereo_data->img_l.cols, CV_8UC1);
            //memcpy(cv_img_left.data, stereo_data->img_l.data, cv_img_left.total() * cv_img_left.elemSize());

            //cv::Mat cv_img_right = cv::Mat(stereo_data->img_r.rows, stereo_data->img_r.cols, CV_8UC1);
            //memcpy(cv_img_right.data, stereo_data->img_r.data, cv_img_right.total() * cv_img_right.elemSize());

            sensor_msgs::ImagePtr image_msg_left;
            sensor_msgs::ImagePtr image_msg_right;

            CreateImageMsg(&image_msg_left, stereo_data->img_l, "StereoLeft", timestamp);
            CreateImageMsg(&image_msg_right, stereo_data->img_r, "StereoRight", timestamp);

            //CreateImageMsg(&image_msg_left, cv_img_left, "StereoLeft", timestamp);
            //CreateImageMsg(&image_msg_right, cv_img_right, "StereoRight", timestamp);

            pub_left.publish(image_msg_left);
            pub_right.publish(image_msg_right);

            //pub_left_cam_info.publish(camera_calib_msg_left);
            //pub_right_cam_info.publish(camera_calib_msg_right);
*/
            }
        }
    }

    ros::shutdown();
    std::terminate();

    return 0;
}
