// Multithread circular buffer

#include <cstddef>
#include <condition_variable>
#include <mutex>
#include <stdexcept>
#include <utility>

template<typename T>
class CircularBuffer {
public:
    explicit CircularBuffer(std::size_t size = 1024)
        : m_buffer(nullptr), m_size(size), m_read(0), m_write(0) {
        if (m_size == 0) throw std::invalid_argument("[CircularBuffer] Invalid buffer size");
        m_buffer = new T[size];
    }

    ~CircularBuffer() {
        delete[] m_buffer;
    }

    inline T read() { // Blocking

        std::unique_lock<std::mutex> lock(m_mutex);
        m_condition.wait(lock, [this]{return m_read != m_write;}); // wait for data to be ready

        // enter critical section
        T element = std::move(m_buffer[m_read]);
        m_read = next(m_read);
        // leave critical section

        lock.unlock();

        return element;
    }

    inline void write(const T& element) { // Non-blocking (avoid context switch)

        while (!m_mutex.try_lock()); // active wait

        // enter critical section
        m_buffer[m_write] = element;
        m_write = next(m_write);
        if (m_read == m_write) m_read = next(m_read);
        // leave critical section

        m_mutex.unlock();

        m_condition.notify_one(); // notify new data ready
    }

private:

    T* m_buffer;
    std::size_t m_size, m_read, m_write;

    std::mutex m_mutex;
    std::condition_variable m_condition;

    inline std::size_t next(std::size_t index) {
        index++;
        if (index == m_size) index = 0;
        return index;
    }
};
