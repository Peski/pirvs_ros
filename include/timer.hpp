// COLMAP - Structure-from-Motion and Multi-View Stereo.
// Copyright (C) 2017  Johannes L. Schoenberger <jsch at inf.ethz.ch>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef TIMER_H_
#define TIMER_H_

#include <chrono>

class Timer {
 public:
  Timer() : started_(false), paused_(false) {}

  void Start() {
    started_ = true;
    paused_ = false;
    start_time_ = std::chrono::high_resolution_clock::now();
  }

  void Restart() {
    started_ = false;
    Start();
  }

  void Pause() {
    paused_ = true;
    pause_time_ = std::chrono::high_resolution_clock::now();
  }

  void Resume() {
    paused_ = false;
    start_time_ += std::chrono::high_resolution_clock::now() - pause_time_;
  }

  void Reset() {
    started_ = false;
    paused_ = false;
  }

  double ElapsedMicroSeconds() const {
    if (!started_) {
      return 0.0;
    }

    using namespace std::chrono;
    if (paused_) {
      return duration_cast<microseconds>(pause_time_ - start_time_).count();
    } else {
      return duration_cast<microseconds>(high_resolution_clock::now() - start_time_).count();
    }
  }

  double ElapsedSeconds() const { return ElapsedMicroSeconds() / 1e6; }
  double ElapsedMinutes() const { return ElapsedSeconds() / 60; }
  double ElapsedHours() const { return ElapsedMinutes() / 60; }

/*
  void PrintSeconds() const;
  void PrintMinutes() const;
  void PrintHours() const;
*/

 private:
  bool started_;
  bool paused_;
  std::chrono::high_resolution_clock::time_point start_time_;
  std::chrono::high_resolution_clock::time_point pause_time_;
};

#endif  // TIMER_H_
