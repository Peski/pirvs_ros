# pirvs_ros
A ROS wrapper for PIRVS SDK to work with PerceptIn V1 devices. Adapted form [IRONSIDES](https://github.com/PerceptIn/IRONSIDES).

**Author:** [David Zuñiga-Noël](http://mapir.isa.uma.es/mapirwebsite/index.php/people/270)

**License:**  [GPLv3](https://raw.githubusercontent.com/dzunigan/calibration2d/master/LICENSE.txt)

## 1. Dependencies (tested)

* CMake (3.5.1-1ubuntu1) `sudo apt install cmake`

* ROS (Kinetic Kame) See [ROS installation](http://wiki.ros.org/kinetic/Installation).

* Boost (1.58.0.1ubuntu1) `sudo apt install libboost-all-dev`

* OpenCV (3.3.1-dev from ROS) See [opencv.org](https://opencv.org/). Recomemeded compile flags: `-DWITH_IPP=OFF -DWITH_LAPACK=OFF -DWITH_CUDA=OFF`

* libusb-1.0 (2:1.0.20-1) `sudo apt install libusb-1.0-0-dev`

* BLAS (3.6.0-2ubuntu2) `sudo apt install libblas-dev`

* LAPACK (3.6.0-2ubuntu2) `sudo apt install liblapack-dev`
   
## 2. Build

Once all dependencies have been installed, proceed to build the source code within a catkin workspace (see [catkin tutorials](http://wiki.ros.org/catkin/Tutorials)):
```
catkin_make --pkg pirvs_ros
```

## 3. Data Format

Monocrome images comming from the stereo camera are published using the *raw* transport to `/pirvs/camera_left/image_raw` and `/pirvs/camera_right/image_raw`, for the left and right cameras respectively.

Linear acceleration and angular velocity comming from the IMU are published to `/pirvs/imu/data` using the standard ROS *sensor_msg/Imu* messages.

## 4. Usage

The `pirvs_ros_node` driver can be started with:
```
rosrun pirvs_ros pirvs_ros_node
```

Alternatively, you can use the launch file provided in [launch/pirvs_ros.launch](launch/pirvs_ros.launch) to run the driver node.

## 5. Troubleshooting

### Grant USB read/write permissions

In order to open the PerceptIn V1 device, read and write permission are required by PIRVS SDK. You can grant such permissions to your device by writing specific udev rules:
```
sudo nano /etc/udev/rules.d/55-perceptin-usb.rules
```
with the following content:
```
# PerceptIn device (2a0b:00f5): read/write permission granted to plugdev group
SUBSYSTEM=="usb", ATTR{idProduct}=="00f5", ATTR{idVendor}=="2a0b", MODE="0660", GROUP="plugdev"
```
where the idVendor:idProduct pair can be optained from the `lsusb` command. Make sure that you are in the `plugdev` group by running `groups`. Finally, apply the new rules with:
```
sudo udevadm control --reload-rules
```

### The raw image transport limitation

While developping this ROS wrapper, we faced segmentation faults with the standard ROS image publishing procedure. Their are related with the dynamic image transport pluging loader. To overcome this problem, we choose to limit the image transport to the default `raw` transport. Moreover, we were unable to properly link with the `raw_publisher.cpp` in the installed ROS package. So, the code of the latets raw image transport is included in this package.

If you know how to link with the raw publisher (should be within `libimage_transport_plugins` ROS library), or think of a better approach, please let me know.

### Poor driver performance

The data acquisition implemented in the PRIVS SDK library is quite limited. They ONLY provide active waits to check for new data. This means that the main ROS driver thread will eat a CPU core. In order to loose as less data as possible, we run two additional threads: one to publish the stereo data and another for the IMU data. The acquired sensor data is buffered in circular buffers with non-blocking writing before publishing.

Suggestions about how to boost the driver performance are welcome.

